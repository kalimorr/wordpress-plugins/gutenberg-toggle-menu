<div class="krr-gutenberg-toggle-menu">
	<?php /* Button to open in header bar */ ?>
	<button class="components-button edit-post-krr-show-menu has-icon hide-if-no-js">
		<span class="dashicons dashicons-menu"></span>
	</button>

	<?php /* Button to close in menu bar */ ?>
	<button class="edit-post-krr-close-menu hide-if-no-js">
		<span class="dashicons dashicons-arrow-left-alt2"></span>
		<span><?= __( 'Collapse menu' ) ?></span>
	</button>
</div>
