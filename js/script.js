(function (win, doc, $) {
	'use strict'

	win.KrrGutenbergToggleMenu = {

		_openButtonSelector: '.edit-post-krr-show-menu',
		_closeButtonSelector: '.edit-post-krr-close-menu',
		_openClass: 'sticky-adminbar-lateral',
		_openSelector: '.sticky-adminbar-lateral',

		/**
		 * Init method
		 */
		init: function () {
			var self  = this,
				$body = $('body');

			if ($body.hasClass('block-editor-page')) {
				this.classes();
				this.move();

				/* Toggle menu on click on the button */
				$(doc).on('click', this._openButtonSelector, function () {
					$body.removeClass('folded');
					$body.toggleClass(self._openClass);
				})

				/* Hide menu on click on the content */
				.on('click', self._openSelector + ' #wpcontent', function () {
					$body.removeClass(self._openClass);
				})

				/* Hide menu on click on the close button */
				.on('click', self._closeButtonSelector, function () {
					$body.removeClass(self._openClass);
				})

				/* Hide menu on Escape */
			  	.on('keydown', function(e){
					if (e.which === 27) {
						$body.removeClass(self._openClass);
					}
				});
			}
		},

		/**
		 * Handle the necessary classes
		 */
		classes: function () {
			var $body = $('body');

			if ($body.hasClass('folded')) {
				$body.removeClass('folded');
			}


			$body.addClass('has-krr-gutenberg-toggle-menu');
		},

		/**
		 * Move toggle button in header
		 */
		move : function() {
			var self = this;

			var $openButton = $(self._openButtonSelector).detach();
			var $closeButton = $(self._closeButtonSelector).detach();

			var checkup = setInterval(function() {
				if ($('.interface-interface-skeleton__header').length > 0) {
					clearInterval(checkup);
					$('.interface-interface-skeleton__header .edit-post-header').prepend($openButton);
				}
			}, 500);

			$('#adminmenumain').append($closeButton);
		}
	};

	/* Admin page ready */
	$(doc).ready(function () {
		KrrGutenbergToggleMenu.init();
	});
})(window, document, jQuery);